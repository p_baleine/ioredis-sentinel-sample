# redis-sentinel-sample

[Redis Sentinel](http://redis.io/topics/sentinel)を用いて自動フェイルオーバーするサンプル。

## 概要

Redisはマスターとスレーブの2台構成、アプリサーバ1台は且つSentinelサーバ、アプリは[ioredis](https://github.com/luin/ioredis)を利用してSentinelに接続し、マスターが落ちたら[ioredis](https://github.com/luin/ioredis)まかせで向き先が変わる

※実際にはSentinelサーバは3台以上必要

## パラメタ

まだあんまりちゃんと調べてないけど、↓ら辺は変更が必要

### `scripts/redis_conf/sentinel/6379.conf`

#### マスタのIPとか

最後の数字`quorum`個の`sentinel`が同意した場合、マスタは落ちていると判断される。

`sentinel monitor mymaster 192.168.33.21 6379 1`

#### マスタがダウンしていると判断するPINGのインターバル

↓だと1秒

`sentinel down-after-milliseconds mymaster 1000`

#### スレーブの場所

これは`sentinel`が勝手に見っけて`/etc/redis/6379.conf`に書き出すらしい…

## 試し方

以下コマンドでVM三つ(`app`、`redis_master`、`redis_slave`)が立ち上がる

```bash
$ git clone http://192.168.101.76:8080/gitbucket/git/tajima-junpei/redis-sentinel-sample.git
$ cd redis-sentinel-sample
$ vagrant up
```

`app`サーバに入る

```bash
$ vagrant ssh app
```

`tmux`立ち上げといて

```bash
$ tmux
```

`redis_master`にログインしてログを確認する

```bash
$ ssh 192.168.33.21
$ tail -f /var/log/redis_6379.log
```

別窓で`redis_slave`にログインしてログを確認する

```bash
$ ssh 192.168.33.22
$ tail -f /var/log/redis_6379.log
```

別窓でローカルのログを確認する

```bash
$ sudo tail -f /var/log/messages
```

別窓で`redis_master`にログインして値入れておく

```bash
$ ssh 192.168.33.21
$ redis-cli
127.0.0.1:6379> set foo bar
OK
127.0.0.1:6379> get foo
"bar"
```

`redis_slave`から値を見てみる

```bash
$ ssh 192.168.33.22
$ redis-cli
127.0.0.1:6379> get foo
"bar"
```

`app`に戻ってプログラムを立ち上げる

```bash
$ cd ~/app/redis-sentinel-test
$ node index.js
null 'bar'
null 'bar'
null 'bar'
...
```

`redis_master`に入ってサーバDOWNさせる、ログ監視しておく

```bash
$ sudo /etc/init.d/redis_6379 stop
```
