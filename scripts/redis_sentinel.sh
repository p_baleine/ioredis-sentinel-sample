# Provisioning script to setup init scripts for Redis and start Redis server with sentinel mode.

sudo mkdir /etc/redis/

sudo cp /home/vagrant/redis_conf/sentinel/6379.conf /etc/redis/
sudo cp /home/vagrant/redis_conf/sentinel/redis_6379 /etc/init.d/
sudo chmod a+x /etc/init.d/redis_6379

sudo chkconfig --add redis_6379
sudo chkconfig --level 345 redis_6379 on

sudo /etc/init.d/redis_6379 start
