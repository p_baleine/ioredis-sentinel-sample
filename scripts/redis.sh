# Provisioning script to install Redis.

REDIS_VERSION=3.0.5

cd ~
sudo yum install -y gcc
curl -LO http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz
sudo tar zxvf redis-${REDIS_VERSION}.tar.gz -C /opt
sudo chown vagrant:vagrant -R /opt/redis-${REDIS_VERSION}
cd /opt/redis-${REDIS_VERSION}
make && sudo make PREFIX=/usr install
