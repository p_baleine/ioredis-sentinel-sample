# Provisioning script to setup Node environment.

# Install tmux.

curl -L -O http://pkgs.repoforge.org/rpmforge-release/rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
sudo rpm -ivh rpmforge-release-0.5.3-1.el6.rf.x86_64.rpm
sudo yum install tmux -y

# Install nvm.

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.23.3/install.sh | bash
source ~/.bashrc

# Install Node.js via nvm.

nvm install 0.10.5
echo 'nvm use 0.10.5' >> ~/.bashrc

# Update nvm.

npm install npm -g
