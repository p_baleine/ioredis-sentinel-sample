var Redis = require('ioredis');

var redis = new Redis({
  sentinels: [{ host: 'localhost', port: 6379 }],
  name: 'mymaster'
});

setInterval(function() {
  redis.get('foo', function(err, res) {
    console.log(err, res);
  });
}, 1000);

